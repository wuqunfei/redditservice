
# Reddit Service  
  
A service can trace and ranking the top trending topics from reddit.com. Service is running on https://redditservice.wuqunfei.usw1.kubesail.io/ as demo.
## Service Pipeline Flows  
  
```mermaid  
graph LR  
A(Reddit)-.1.api/trending_subreddits.json.->TrackTopics.Task  
A(Reddit)-.1.r/trendingsubreddits.->TrackTopics.Task  
TrackTopics.Task--2.writeTopTopics-->topics.txt  
topics.txt--3.reading-->RankTopics.Task  
RankTopics.Task --4.request--> Reddit  
RankTopics.Task--5.save-->sqlite  
```  
There are 2  tasks,  **TrackTopics.task** and **RankTopics.task** for 5 steps processing  
  
 - **TrackTopics.task** request trending subreddits(Topics) from Reddit  
 -  [x ] REST call https://www.reddit.com/api/trending_subreddits.json  
 - [] Request [https://www.reddit.com/r/trendingsubreddits/](https://www.reddit.com/r/trendingsubreddits/) to get the hot topic from link comments.  
 - **TrackTopics.task** write the topic names into YYYY-MM-DD HH:MM:SS.topics.txt file  
 -  **RankTopics.task** read topics.txt   
 - **RankTopics.task** request to get Top 50 posts and reqeust Comments for each Post to Reddit  
 - **RankTopics.task** caulate the socre of each topic and save into local sqlite file(ranking.db)   
  
*Refer Reddit API: [https://www.reddit.com/dev/api#GET_api_trending_subreddits](https://www.reddit.com/dev/api#GET_api_trending_subreddits)  Currently, the application using the API endpoint to get the top trending, limited 5 subreddits by API.
  
## Luigi pros vs cons.
Generally, Luigi is a simple and nice framework to build complicated pipeline after 2 days analysis,  there are 3 points need to consider as **disadvantages**  as following
 - Does not provide a way to trigger flows, need **extra** scheduler service or cronjob   
 - Does not native support distributed execution  
 - Does not support other languages, only support python
  
  
## How to run a task on local/remote/schedule?  
  
To make sure your python enviroment >=3.7,  or using [https://virtualenv.pypa.io/en/latest/](https://virtualenv.pypa.io/en/latest/) by your own.  
```  
$ git clone git@gitlab.com:wuqunfei/redditservice.git  
$ cd redditservice  
$ pip3.7 install requirements.txt  
$ python3.7 app.py [local|remote|scheduled]
```  
Run Local Mode
```  
def run_local_task():    
    ranking_task = RankTopics()    
    luigi.build([ranking_task], workers=1, local_scheduler=True)    
 ```  
Run Remote Mode
```  
def run_remote_task():    
    host = '0.0.0.0'  
    port = 8082    ranking_task = RankTopics()    
    luigi.build([ranking_task], local_scheduler=False, scheduler_host=host, scheduler_port=port)  
```  
  
  
## How to config the top subredits/posts/comments?  
  
there is a **luigi.cfg** file in the root folder as example.  
```  
# Reddit developer 

crendential client_id = xxx  
client_secret = xxx-yyyy-zzz  
  
# Top subredddit(topic) limit  
# Top post limit for each subreddit  
# Top comments limit for each post  
  
subreddits_limit = 5  
posts_limit = 10  
comments_limit = 5  
  
```  
## How to retry failed task?  
Keep worker alive firstly,  set retry time and delay for each retry. For example: retray 3 time and which time wait for 600 seconds. If the work quits in the end, Luigi saved the status of the task,  it will try the failed job automatically. 
```
[worker]  
keep_alive = true

[scheduler]  
retry_count = 3  
retry_delay = 600  
```
## How to trigger the scheduler task?  
  
There is no trigger in Luigi to run a scheduler task. 
 - We can use [https://apscheduler.readthedocs.io/en/latest/](https://apscheduler.readthedocs.io/en/latest/) as a extra scheduler dynamically in python application. 

```
from apscheduler.schedulers.background import BlockingScheduler
scheduler = BlockingScheduler()
@scheduler.scheduled_job('cron', hour='10,20')  
def ranking_scheduled_task():  
    """ Run Job twice everyday locally, at 10:00 and 20:00 """  
    cmd = "luigi --module  redditservice/workflow RankTopics --local-scheduler"  
    stout = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read()
    print(stout)
scheduler.start()
```

 - Using system crontab job on docker instance
```
$ crontab e
$ 0 10,20 * * * cd /usr/src/app && luigi --module redditservice/workflow RankTopics
```  


  
## Database Schema  
when running the local task,  there is a file named**ranking.db** generated after the tasked completed. Using a SQLite tool like https://sqlitebrowser.org/ to check the results  
  
```  
CREATE TABLE "reddit_ranking" (  
 "tacking_time" DATETIME NOT NULL, 
 "topic_name"   VARCHAR NOT NULL, 
 "score"        FLOAT NOT NULL);  
```  
## Software CI/CD Pipeline 
Using dockerise our application, register on Repository,  kubernetes platform to run it,  some tech stacks as follow

 - Pipeline Gitlab https://gitlab.com/wuqunfei/redditservice/pipelines
 -  Docker Hub [https://cloud.docker.com/u/coffeepay/repository/docker/coffeepay/redditservice](https://cloud.docker.com/u/coffeepay/repository/docker/coffeepay/redditservice)
 -  Kubernetes [https://kubesail.com/](https://kubesail.com/)

  
```mermaid  
graph LR  
SourceCode-- Code Check  -->Pylint
SourceCode-- Unit/It Test  -->PyTest
Pylint-->ImagePackage  
PyTest-- docker build -->ImagePackage  
ImagePackage-- docker register -->ImageRegister 
ImageRegister-- kubernetes Deploy-->Deploy
Deploy-->e2etest
```  
For example,  1 CI and CD job,[https://gitlab.com/wuqunfei/redditservice/pipelines/90086820](https://gitlab.com/wuqunfei/redditservice/pipelines/90086820)

Code Steps in  https://gitlab.com/wuqunfei/redditservice/blob/master/.gitlab-ci.yml pipeline
```
$ pylint *.py
# check code

$ pytest --cov=redditservice .
# unit test and coverage

$ docker build -t redditservice .
# build code as Luigid server docker image

$ docker push redditservice
# register the image into repository

$ kubectl run --image=redditservice redditluigid --port=8082
# pull the docker image and run on kubernetes platform

$ python app.py remote
# E2E test with real remote server
```


