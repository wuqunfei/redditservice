import json
import logging
import os

import pytest
import requests_mock

from redditservice.crawler import CrawlerService

# Constants

CLIENT_ID = "YGeQjH6PnHU6rQ"
CLIENT_SECRET = "S2GQQ7Xzw92u_hOdlBwqXWOcsLw"
TOPICS = ["nonmurdermysteries", "ThePurgeTV", "AnimalsBeingSleepy", "catswhotrill", "behindthegifs"]
TRENDING_URL = "https://www.reddit.com/api/trending_subreddits.json"
TOPIC_NAME = "Hedgehog"
ACCESS_TOKEN = "-7T56cKJ0lX4CkMlIoiA1wOUmW74"

logging.basicConfig(handlers=[logging.StreamHandler()], level=logging.DEBUG,
                    format="%(asctime)s %(levelname)s [%(filename)s %(lineno)d]: %(message)s")


# Fixtures

@pytest.fixture(scope="function", autouse=True, name="crawler")
def get_crawler():
    crawler_service = CrawlerService()
    crawler_service.set_client_credential(client_id=CLIENT_ID, client_secret=CLIENT_SECRET)
    return crawler_service


# Test Cases
def test_set_client_credential(crawler):
    assert CLIENT_ID == crawler.client_id
    assert CLIENT_SECRET == crawler.client_secret
    assert crawler.client is not None


def test_get_trending_topics_when_reddit_response_ok(crawler):
    with requests_mock.Mocker() as request_mocker:
        # given
        request_mocker.get(url=TRENDING_URL, status_code=200, json={"subreddit_names": TOPICS})
        # when
        topics = crawler.get_trending_topics(limit=5)
        # then
        assert topics == TOPICS


def test_get_trending_empty_topic_when_reddit_response_without_sureddit_names(crawler):
    with requests_mock.Mocker() as request_mocker:
        # given
        request_mocker.get(url=TRENDING_URL, status_code=200, json={})
        # when
        topics = crawler.get_trending_topics(limit=5)
        # then
        assert topics == []


def test_get_trending_empty_topic_when_reddit_response_not_ok(crawler):
    with requests_mock.Mocker() as request_mocker:
        # given
        request_mocker.get(url=TRENDING_URL, status_code=500)
        # when
        topics = crawler.get_trending_topics(limit=5)
        # then
        assert topics == []


def test_get_top_submissions(crawler):
    current_path = os.path.dirname(__file__)
    file_name = current_path + "/response/subreddit_top_10.json"
    with requests_mock.Mocker() as requests_mocker, open(file_name) as response_file:
        # given
        response_json = json.load(response_file)
        mock_post_api_response(requests_mocker, response_json)
        posts_limit = 10
        # when
        posts = crawler.get_top_submissions(TOPIC_NAME, limit=posts_limit)
        # then
        assert len(posts) == posts_limit


def test_top_comment_scores(crawler):
    current_path = os.path.dirname(__file__)
    with requests_mock.Mocker() as requests_mocker, \
            open(current_path + "/response/subreddit_top_1.json") as response_posts_file, \
            open(current_path + "/response/comments.json") as response_comments_file:
        # given
        posts_json = json.load(response_posts_file)
        comments_json = json.load(response_comments_file)
        mock_post_api_response(requests_mocker, posts_json)
        mock_comment_api_response(requests_mocker, comments_json)

        posts_limit = 1
        comments_limit = 5
        # when
        posts = crawler.get_top_submissions(TOPIC_NAME, limit=posts_limit)
        scores = crawler.get_top_comment_scores(posts[0], limit=comments_limit)
        # then
        assert len(posts) == posts_limit
        assert len(scores) == comments_limit

def mock_post_api_response(requests_mocker, response_json):
    requests_mocker.post(url="https://www.reddit.com/api/v1/access_token",
                         json={"access_token": ACCESS_TOKEN, "token_type": "bearer", "expires_in": 3600,
                               "scope": "*"})
    subreddit_top_request_url = "https://oauth.reddit.com/r/%s/top" % TOPIC_NAME
    requests_mocker.get(url=subreddit_top_request_url, headers={'Authorization': 'bearer ' + ACCESS_TOKEN},
                        json=response_json)


def mock_comment_api_response(requests_mocker, response_json):
    comments_request_url = "https://oauth.reddit.com/comments/an30m2/"
    requests_mocker.get(url=comments_request_url, headers={'Authorization': 'bearer ' + ACCESS_TOKEN},
                        json=response_json)
