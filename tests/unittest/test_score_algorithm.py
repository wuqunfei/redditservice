import logging

import pytest

from redditservice.workflow import RankTopics

logging.basicConfig(handlers=[logging.StreamHandler()], level=logging.DEBUG,
                    format="%(asctime)s %(levelname)s [%(filename)s %(lineno)d]: %(message)s")


# Fixtures
@pytest.fixture(scope="function", name="task")
def get_task():
    # given
    client_id = "YGeQjH6PnHU6rQ"
    client_secret = "S2GQQ7Xzw92u_hOdlBwqXWOcsLw"
    posts_limit = 1
    comments_limit = 5
    return RankTopics(posts_limit=posts_limit,
                      comments_limit=comments_limit,
                      client_id=client_id,
                      client_secret=client_secret)


# test cases
def test_calculate_1_topic_with_2_post_each_4_comments(task, mocker):
    """
    formula:  ((1 + 2 + 3 + 4)/4 + (1 + 2 + 3 + 4)/4)/2 = 2.5
    """
    # given
    topic_name = "random_topic_name"
    post_amount = 2
    comment_scores = [1, 2, 3, 4]
    with mocker.patch.object(task.crawler, 'get_top_submissions', return_value=range(post_amount)), \
         mocker.patch.object(task.crawler, 'get_top_comment_scores', return_value=comment_scores):
        # when
        score = task.get_score(topic_name)

        # then
        assert score == 2.5
