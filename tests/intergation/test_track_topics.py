import logging
import os
from unittest import TestCase

import luigi

from redditservice.workflow import TrackTopics

logging.basicConfig(handlers=[logging.StreamHandler()], level=logging.DEBUG,
                    format="%(asctime)s %(levelname)s [%(filename)s %(lineno)d]: %(message)s")


class TestTrackTopics(TestCase):

    def setUp(self):
        self._clean_file()

    def tearDown(self):
        self._clean_file()

    def test_write_file(self):
        # given
        subreddits_limit = 5
        client_id = "YGeQjH6PnHU6rQ"
        client_secret = "S2GQQ7Xzw92u_hOdlBwqXWOcsLw"

        # when
        task = TrackTopics(subreddits_limit=subreddits_limit, client_id=client_id, client_secret=client_secret)
        luigi.build([task], workers=12, local_scheduler=True)

        # then
        self.assertEqual(task.complete(), True)
        self.assertIsNotNone(self._find_topics_file())

    def _clean_file(self):
        topic_file = self._find_topics_file()
        if topic_file:
            os.remove(os.getcwd() + "/" + topic_file)

    @staticmethod
    def _find_topics_file():
        topic_file = None
        current_path = os.getcwd()
        for file_name in os.listdir(current_path):
            if file_name.find("topics.txt") != -1:
                topic_file = file_name
        return topic_file
