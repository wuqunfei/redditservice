import logging
import os
import sqlite3
from unittest import TestCase

import luigi

from redditservice.workflow import RankTopics

logging.basicConfig(handlers=[logging.StreamHandler()], level=logging.DEBUG,
                    format="%(asctime)s %(levelname)s [%(filename)s %(lineno)d]: %(message)s")


class TestRankTopics(TestCase):

    def setUp(self):
        self._clean_file()
        self.db_connection = "ranking.db"

    def tearDown(self):
        self._clean_file()

    def test_write_db(self):
        # given
        client_id = "YGeQjH6PnHU6rQ"
        client_secret = "S2GQQ7Xzw92u_hOdlBwqXWOcsLw"
        posts_limit = 1
        comments_limit = 2

        # when
        task = RankTopics(posts_limit=posts_limit,
                          comments_limit=comments_limit,
                          client_id=client_id,
                          client_secret=client_secret)

        luigi.build([task], workers=12, local_scheduler=True)

        # then
        self.assertEqual(task.complete(), True)
        self.assertIsNotNone(self._find_topics_file())
        self.assertIsNotNone(self.find_db_file())
        self.assertGreater(len(self.find_rows(self.db_connection)), 0)

    def _clean_file(self):
        topic_file = self._find_topics_file()
        current_path = os.getcwd()
        if topic_file:
            os.remove(current_path + "/" + topic_file)
        db_file = self.find_db_file()
        if db_file:
            os.remove(current_path + "/" + db_file)

    @staticmethod
    def _find_topics_file():
        topic_file = None
        current_path = os.getcwd()
        for file_name in os.listdir(current_path):
            if file_name.find("topics.txt") != -1:
                topic_file = file_name
        return topic_file

    @staticmethod
    def find_db_file():
        db_file = None
        current_path = os.getcwd()
        for file_name in os.listdir(current_path):
            if file_name.find(".db") != -1:
                db_file = file_name
        return db_file

    @staticmethod
    def find_rows(db_connection):
        connection, cursor = None, None
        rows = []
        try:
            connection = sqlite3.connect(db_connection)
            cursor = connection.cursor()
            table_name = RankTopics.table
            cursor.execute("SELECT * FROM %s" % table_name)
            rows = cursor.fetchall()
        except sqlite3.Error as ex:
            logging.error(ex)
        finally:
            if cursor:
                cursor.close()
            if connection:
                connection.close()
        return rows
