import logging
import os
import subprocess
import sys

import luigi
from apscheduler.schedulers.background import BlockingScheduler

from redditservice.workflow import RankTopics
from tests.intergation.test_rank_topics import TestRankTopics

logging.basicConfig(handlers=[logging.StreamHandler()], level=logging.DEBUG,
                    format="%(asctime)s %(levelname)s [%(filename)s %(lineno)d]: %(message)s")


# helper functions
def query_results():
    logging.info("Current trending from https://www.reddit.com/api/trending_subreddits.json")
    logging.info("scores db results from ranking.db, UTC Time")

    database_location = "ranking.db"
    rows = TestRankTopics.find_rows(database_location)
    for row in rows:
        logging.info(row)


def reset_db():
    if TestRankTopics.find_db_file():
        os.remove(os.getcwd() + "/ranking.db")


# 3 kind run mode

def run_local_task():
    ranking_task = RankTopics()
    logging.info("Running local Task now ...")
    luigi.build([ranking_task], workers=12, local_scheduler=True)
    logging.info("Running local Task completed %s", ranking_task.complete())
    query_results()


def run_remote_task():
    """
    Set host and port, run task on a luigid server remotely
    """
    host = 'redditservice.wuqunfei.usw1.kubesail.io'
    port = 80
    ranking_task = RankTopics()
    luigi.build([ranking_task], workers=12, local_scheduler=False, scheduler_host=host, scheduler_port=port)
    query_results()


def run_scheduled_task():
    """
     Run Job twice everyday locally, at 10:00 and 20:00
    """
    cmd = "luigi --module  redditservice/workflow RankTopics --local-scheduler"
    stout = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read()
    logging.info(stout)


if __name__ == "__main__":
    reset_db()

    if len(sys.argv) > 1:
        MODE = sys.argv[1]
        if MODE == "local":
            run_local_task()
        elif MODE == "remote":
            run_remote_task()
        elif MODE == "scheduled":
            SCHEDULER = BlockingScheduler()
            SCHEDULER.add_job(run_scheduled_task(), 'cron', hour='10,20')
            SCHEDULER.start()
    else:
        run_local_task()
