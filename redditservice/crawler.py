import praw
import requests


class CrawlerService:

    def __init__(self):
        self.trending_url = "https://www.reddit.com/api/trending_subreddits.json"
        self.user_agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36"

        self.client_id = None
        self.client_secret = None
        self.client = None

    def set_client_credential(self, client_id, client_secret):
        self.client_id = client_id
        self.client_secret = client_secret
        self.client = praw.Reddit(client_id=self.client_id,
                                  client_secret=self.client_secret,
                                  user_agent=self.user_agent)

    def get_trending_topics(self, limit):
        """
        https://www.reddit.com/dev/api/#GET_api_trending_subreddits

        1. get trending by json endpoint, only 5 subtrending (No Authentication)
        2. get trending by reading r/trendingsubreddits link (Need Authentication)
        """
        return self._get_trending_subreddits_by_endpoint(limit)
        # return self._get_trending_subreddits_by_trending_link(limit)

    def get_top_submissions(self, subreddit_name, limit):
        subreddit = self.client.subreddit(subreddit_name)
        return list(subreddit.top(limit=limit))

    def get_top_comment_scores(self, submission, limit):
        scores = list(map(self._extract_score, list(submission.comments)))
        scores.sort(reverse=True)
        return scores[:limit]

    # def _get_trending_subreddits_by_trending_link(self, limit):
    #     subreddit = self.client.subreddit('trendingsubreddits')
    #     hot_post_link = limit / 5.0 * 2  # 1 link has 5 topic names, everyday 2 time same link
    #     submissions = list(subreddit.hot(limit=hot_post_link))
    #     topics = set()
    #     for submission in submissions:
    #         for topic in self._extract_topics(submission):
    #             if len(topics) <= limit:
    #                 topics.add(topic)
    #     return topics

    def _get_trending_subreddits_by_endpoint(self, limit):
        topics = []
        response = requests.get(self.trending_url, headers={'user-agent': self.user_agent}, data={'limit': limit})
        if response.status_code == 200:
            response_topics = response.json().get('subreddit_names')
            if response_topics is not None:
                topics = response_topics
        return topics

    @staticmethod
    def _extract_score(comment):
        score = 0
        if comment is not None and hasattr(comment, 'score'):
            score = comment.score
        return score

    @staticmethod
    def _extract_topics(submission):
        results = submission.title.split("/r/")[1:]
        return list(map(lambda item: item.replace(",", "").strip(), results))
