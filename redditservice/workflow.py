import datetime

import luigi
from luigi.contrib import sqla
from sqlalchemy import DateTime, String, Float

from redditservice.crawler import CrawlerService


class TrackTopics(luigi.Task):
    tracking_time = luigi.DateSecondParameter(default=datetime.datetime.now())
    subreddits_limit = luigi.IntParameter(default=5)
    client_id = luigi.Parameter()
    client_secret = luigi.Parameter()
    crawler = CrawlerService()

    def requires(self):
        return None

    def output(self):
        file_name = "%s.topics.txt" % self.tracking_time
        return luigi.LocalTarget(file_name)

    def run(self):
        self.crawler.set_client_credential(self.client_id, self.client_secret)

        with self.output().open('w') as out_file:
            topics = self.get_topics()
            for topic in topics:
                out_file.write(topic + '\n')

    def get_topics(self):
        return self.crawler.get_trending_topics(self.subreddits_limit)


class RankTopics(sqla.CopyToTable):
    posts_limit = luigi.IntParameter()
    comments_limit = luigi.IntParameter()

    # http client setting
    client_id = luigi.Parameter()
    client_secret = luigi.Parameter()
    crawler = CrawlerService()

    # sqlite setting
    reflect = True
    echo = False
    connection_string = "sqlite:///ranking.db"
    columns = [
        (['tacking_time', DateTime(timezone=True)], {"nullable": False}),
        (['topic_name', String], {"nullable": False}),
        (['score', Float], {"nullable": False, "default": 0})
    ]
    table = "reddit_ranking"

    def requires(self):
        return TrackTopics(client_id=self.client_id, client_secret=self.client_secret)

    def rows(self):

        self.crawler.set_client_credential(self.client_id, self.client_secret)
        with self.input().open() as in_file:
            for topic_name in in_file:
                topic_name = topic_name.replace('\n', '')
                row = (datetime.datetime.utcnow(), topic_name, self.get_score(topic_name))
                yield row

    def get_score(self, topic_name):
        """
        give a topic_name (sub reddit) , return the score of the topic

        algorithm:
                subRS = ∑ postRs/n
                postRS = ∑ commentScore(CP)/n

        :param topic_name
        :return: score
        """
        sub_reddit_score = 0
        posts_scores = []
        for post in self.crawler.get_top_submissions(topic_name, self.posts_limit):
            try:
                comment_scores = self.crawler.get_top_comment_scores(post, self.comments_limit)
                if len(comment_scores) > 0:
                    post_rs = sum(comment_scores) / len(comment_scores)
                    posts_scores.append(post_rs)
            except Exception as ex:
                print(ex)
        if len(posts_scores) > 0:
            sub_reddit_score = sum(posts_scores) / len(posts_scores)
        return sub_reddit_score
