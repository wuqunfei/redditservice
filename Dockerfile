FROM python:3.7-slim-buster
WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt

COPY . /usr/src/app

EXPOSE 8082
CMD ["luigid"]

# need to add job in the docker
# 0 10,20 * * * cd /usr/src/app && luigi --module redditservice/workflow RankTopics
